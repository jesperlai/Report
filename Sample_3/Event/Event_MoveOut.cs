﻿using Sample_3.Models;
using Sample_3.ViewModels;
using System.Collections.Generic;
using System.Linq;

namespace Sample_3.Event
{
    public class Event_MoveOut : BaseEvent
    {
        public Event_MoveOut(BuildEventParam param) : base(param)
        {

        }

        public override List<TriggerPointVM> GetTriggerPoint()
        {
            var txns = repo.GetTxn(builderParam.STime, builderParam.ETime, "進站");

            var triggerPoints = from data in txns
                                group data by new { data.Lot_No } into g
                                let firstOne = g.OrderBy(q => q.Txn_Dt).First()  //假設進站要報最早
                                select new TriggerPointVM
                                {
                                    Lot_No = g.Key.Lot_No,
                                    Stage_Id = firstOne.Stage_Id,
                                    Qty = firstOne.Qty,
                                };

            return triggerPoints.ToList();
        }

        public override List<Report> GetData(List<TriggerPointVM> triggerPoints)
        {
            var result = base.GetData(triggerPoints);

            //處理一些該 event 特別不同於 base event 的地方
            foreach (var item in result)
            {
                item.Qty -= 1;   //隨便舉例 假設進站數量都固定要 - 1 

                //通常還會有更多更多相異處
                //通常還會有更多更多相異處
                //通常還會有更多更多相異處
                //通常還會有更多更多相異處
            }

            return result;
        }
    }
}
