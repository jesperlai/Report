﻿using Sample_3.Models;
using Sample_3.ViewModels;
using System.Collections.Generic;

namespace Sample_3.Event
{
    public class Event_Snap : BaseEvent
    {
        public Event_Snap(BuildEventParam param) : base(param)
        {

        }

        public override List<TriggerPointVM> GetTriggerPoint()
        {
            var snaps = repo.GetSnap();

            var triggerPoints = new List<TriggerPointVM>();
            foreach (var snap in snaps)
            {
                var item = new TriggerPointVM
                {
                    Lot_No = snap.Lot_No,
                    Stage_Id = snap.Stage_Id,
                    Qty = snap.Qty
                };

                triggerPoints.Add(item);
            }

            return triggerPoints;
        }

        public override List<Report> GetData(List<TriggerPointVM> triggerPoints)
        {
            var result = base.GetData(triggerPoints);

            //處理一些該 event 特別不同於 base event 的地方
            foreach (var item in result)
            {
                item.Qty += 1;   //隨便舉例 假設進站數量都固定要 + 1 

                //通常還會有更多更多相異處
                //通常還會有更多更多相異處
                //通常還會有更多更多相異處
                //通常還會有更多更多相異處
            }

            return result;
        }
    }
}
