﻿namespace Sample_3.ViewModels
{
    public class TriggerPointVM
    {
        /// <summary>
        /// 批號
        /// </summary>
        public string Lot_No { get; set; }
        /// <summary>
        /// 站別
        /// </summary>
        public string Stage_Id { get; set; }
        /// <summary>
        /// 過站數量
        /// </summary>
        public double? Qty { get; set; }
    }
}
