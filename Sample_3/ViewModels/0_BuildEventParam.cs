﻿using System;

namespace Sample_3.ViewModels
{
    public class BuildEventParam
    {
        public DateTime STime { get; set; }
        public DateTime ETime { get; set; }
        public string EventName { get; set; }
    }
}
