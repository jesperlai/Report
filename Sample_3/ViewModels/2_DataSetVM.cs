﻿using Sample_3.Models;
using System.Collections.Generic;

namespace Sample_3.ViewModels
{
    public class DataSetVM
    {
        public List<Sales_Order> So { get; set; }
    }
}
