﻿using Sample_3.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Sample_3.Helper
{

    public class Repository
    {
        public List<Lot_Txn> GetTxn(DateTime sTime, DateTime eTime, string txnCode)
        {
            //為了方便講解 (實際應該在DB中)-------------------------------------------------------------------------------
            var txns = new List<Lot_Txn>
            {
                new Lot_Txn { Txn_Dt = DateTime.Parse("2001/1/1"), Lot_No = "貨批1", Txn_Code = "進站", Stage_Id = "A站", Qty = 10 },
                new Lot_Txn { Txn_Dt = DateTime.Parse("2001/1/2"), Lot_No = "貨批1", Txn_Code = "出站", Stage_Id = "A站", Qty = 10 },
                new Lot_Txn { Txn_Dt = DateTime.Parse("2001/1/3"), Lot_No = "貨批1", Txn_Code = "進站", Stage_Id = "B站", Qty = 10 },
                new Lot_Txn { Txn_Dt = DateTime.Parse("2001/1/4"), Lot_No = "貨批1", Txn_Code = "出站", Stage_Id = "B站", Qty = 10 },

                new Lot_Txn { Txn_Dt = DateTime.Parse("2001/2/1"), Lot_No = "貨批2", Txn_Code = "進站", Stage_Id = "A站", Qty = 20 },
                new Lot_Txn { Txn_Dt = DateTime.Parse("2001/2/2"), Lot_No = "貨批2", Txn_Code = "出站", Stage_Id = "A站", Qty = 20 },
                new Lot_Txn { Txn_Dt = DateTime.Parse("2001/2/3"), Lot_No = "貨批2", Txn_Code = "進站", Stage_Id = "B站", Qty = 20 },
                new Lot_Txn { Txn_Dt = DateTime.Parse("2001/2/4"), Lot_No = "貨批2", Txn_Code = "出站", Stage_Id = "B站", Qty = 20 },
                new Lot_Txn { Txn_Dt = DateTime.Parse("2001/2/5"), Lot_No = "貨批2", Txn_Code = "進站", Stage_Id = "C站", Qty = 20 },
                new Lot_Txn { Txn_Dt = DateTime.Parse("2001/2/6"), Lot_No = "貨批2", Txn_Code = "出站", Stage_Id = "C站", Qty = 20 },
            };
            //------------------------------------------------------------------------------------------------------------

            return txns.Where(q => q.Txn_Dt >= sTime && q.Txn_Dt < eTime && q.Txn_Code == txnCode).ToList();
        }

        public List<Lot_Snap> GetSnap()
        {
            //為了方便講解 (實際應該在DB中)-------------------------------------------------------------------------------
            var snaps = new List<Lot_Snap>
            {
                new Lot_Snap { Lot_No = "貨批1", Stage_Id = "B站", Qty = 10 },
                new Lot_Snap { Lot_No = "貨批2", Stage_Id = "C站", Qty = 20 },
            };
            //------------------------------------------------------------------------------------------------------------

            return snaps;
        }

        public List<Sales_Order> GetSo(List<string> lotNos)
        {
            //為了方便講解 (實際應該在DB中)-------------------------------------------------------------------------------
            var sos = new List<Sales_Order>
            {
                new Sales_Order { Lot_No = "貨批1", Po_No = "訂單1", Qty = 50, Sod = DateTime.Parse("2001/1/7") },
                new Sales_Order { Lot_No = "貨批2", Po_No = "訂單2", Qty = 80, Sod = DateTime.Parse("2001/2/7"), Rsod = DateTime.Parse("2001/2/10") },
            };
            //------------------------------------------------------------------------------------------------------------

            return sos.Where(q => q.Lot_No != null && lotNos.Contains(q.Lot_No)).ToList();
        }
    }
}
