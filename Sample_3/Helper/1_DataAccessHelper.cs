﻿using System;

namespace Sample_3.Helper
{
    public partial class DataAccessHelper
    {
        private Repository _repo;

        /// <summary>
        /// 通常會傳入一些參數 為了在資料庫裡撈出一些符合的結果來寫出成報表 ex: 撈某一個區間的資料
        /// </summary>
        public DataAccessHelper(Repository pRepo)
        {
            _repo = pRepo;
        }
    }
}
