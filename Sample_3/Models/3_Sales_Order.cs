﻿using System;

namespace Sample_3.Models
{
    /// <summary>
    /// 訂單資料表
    /// </summary>
    public class Sales_Order
    {
        /// <summary>
        /// 批號
        /// </summary>
        public string Lot_No { get; set; }
        /// <summary>
        /// 原交期
        /// </summary>
        public DateTime Sod { get; set; }
        /// <summary>
        /// 新交期
        /// </summary>
        public DateTime? Rsod { get; set; }
        /// <summary>
        /// 訂單編號
        /// </summary>
        public string Po_No { get; set; }
        /// <summary>
        /// 訂單數量
        /// </summary>
        public int Qty { get; set; }
    }
}
