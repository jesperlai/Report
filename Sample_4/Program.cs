﻿using Sample_4.Event;
using Sample_4.Event.MoveOut;
using Sample_4.Event.Snap;
using Sample_4.Models;
using Sample_4.ViewModels;
using System;
using System.Collections.Generic;

namespace Sample_4
{
    class Program
    {
        private static BuildEventParam MakeBuildEventParam(string eventName)
        {
            return new BuildEventParam
            {
                //為了方便講解 (通常這兩個值會從外面傳進來)---------
                STime = DateTime.Parse("2001/1/1"),
                ETime = DateTime.Parse("2001/2/1"),
                //--------------------------------------------------
                EventName = eventName,
            };
        }

        static void Main(string[] args)
        {
            var result = new List<Report>();

            //快照
            BaseEvent<Lot_Snap> snap = new Event_Snap(MakeBuildEventParam("這是一個快照事件"));
            result.AddRange(snap.Run());

            //出站
            BaseEvent<Lot_Txn> moveOut = new Event_MoveOut(MakeBuildEventParam("這是一個出站事件"));
            result.AddRange(moveOut.Run());

            //寫出檔案
            RptService myRpt = new RptService();
            myRpt.GenFile(result, @"D:\sample.csv");
        }
    }
}
