﻿using Sample_4.Models;
using System.Collections.Generic;

namespace Sample_4.ViewModels
{
    public class DataSetVM
    {
        public List<Sales_Order> So { get; set; }
    }
}
