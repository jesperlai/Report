﻿using System;

namespace Sample_4.Extensions
{
    /// <summary>
    /// 有時後把一些函式包裝成擴充方法使用起來會比較直覺，程式碼看起來也比較乾淨
    /// </summary>
    public static class MyExtensions
    {
        public static int Trim(this double? value)
        {
            return Convert.ToInt32(value);
        }
    }
}
