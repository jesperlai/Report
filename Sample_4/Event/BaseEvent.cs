﻿using Sample_4.Models;
using Sample_4.ViewModels;
using System.Collections.Generic;

namespace Sample_4.Event
{
    public abstract class BaseEvent<T>
    {
        protected BaseDataAccessHelper<T> _helper;
        protected Repository repo;
        protected BuildEventParam builderParam;

        public BaseEvent(BuildEventParam param)
        {
            builderParam = param;
            repo = new Repository();
        }

        public List<Report> Run()
        {
            var triggerPoint = GetTriggerPoint();
            return GetData(triggerPoint);
        }

        public abstract List<T> GetTriggerPoint();

        public abstract List<Report> GetData(List<T> triggerPoints);
    }
}
