﻿using Sample_4.ViewModels;
using System.Collections.Generic;

namespace Sample_4.Event
{
    public abstract class BaseDataAccessHelper<T>
    {
        protected Repository _repo;

        /// <summary>
        /// 通常會傳入一些參數 為了在資料庫裡撈出一些符合的結果來寫出成報表 ex: 撈某一個區間的資料
        /// </summary>
        public BaseDataAccessHelper(Repository pRepo)
        {
            _repo = pRepo;
        }

        public abstract DataSetVM ConcreteDataSet(List<T> triggerPoints);
    }
}
