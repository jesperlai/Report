﻿using Sample_4.Extensions;
using Sample_4.Models;
using Sample_4.Utility;
using Sample_4.ViewModels;
using System.Collections.Generic;

namespace Sample_4.Event.Snap
{
    public class Event_Snap : BaseEvent<Lot_Snap>
    {
        public Event_Snap(BuildEventParam param) : base(param)
        {
            _helper = new SnapHelper(repo);
        }

        public override List<Lot_Snap> GetTriggerPoint()
        {
            var snaps = repo.GetSnap();

            return snaps;
        }

        public override List<Report> GetData(List<Lot_Snap> triggerPoints)
        {
            //完全做自己的事 沒有共用的部份了
            var dataSet = _helper.ConcreteDataSet(triggerPoints);

            var result = new List<Report>();
            //我把Trigger Point 觸發點 縮寫成tp
            foreach (var tp in triggerPoints)
            {
                //該回合需用到的資料 我把this round 縮寫成 tRound
                var tRound = SnapHelper.GetThisRoundData(dataSet, tp);

                //Controller 盡量扁平 如果有複雜的運算可以放到 Utility 做
                var item = new Report
                {
                    Lot_No = tp.Lot_No,
                    Stage_Id = tp.Stage_Id,
                    Qty = tp.Qty.Trim() + 1,   //有些轉型或是常用的運算 寫成extension會比較易用
                    Po_No = tRound.So.Po_No,
                    Order_Qty = tRound.So.Qty,
                    Sod = DataUtility.GetSod(tRound.So),  //假設這個欄位有很複雜的邏輯 放到Utitlity做
                    Event_Name = builderParam.EventName,
                };

                result.Add(item);
            }

            return result;
        }
    }
}
