﻿using Sample_4.Models;
using Sample_4.ViewModels;
using System.Collections.Generic;
using System.Linq;

namespace Sample_4.Event.MoveOut
{
    public partial class MoveOutHelper : BaseDataAccessHelper<Lot_Txn>
    {
        public MoveOutHelper(Repository pRepo) : base(pRepo)
        {
        }

        /// <summary>
        /// 其他資料都是based on 你觸發點撈出來的資料有什麼 再去補撈其他資料 
        /// 例如: 先撈出過站記錄，再透過這段時間內有哪些貨批在動 (表示要報) 再去撈出這些貨批的客戶訂單資料
        /// </summary>
        public override DataSetVM ConcreteDataSet(List<Lot_Txn> triggerPoints)
        {
            DataSetVM dataSet = new DataSetVM();

            dataSet.So = GetSo(triggerPoints);

            return dataSet;
        }

        private List<Sales_Order> GetSo(List<Lot_Txn> triggerPoints)
        {
            //通常表示你會有專屬於 MoveOut 的取 So 的條件
            var lotNos = triggerPoints.Select(q => q.Lot_No).Distinct().ToList();
            var result = _repo.GetSo(lotNos);

            return result;
        }
    }
}
