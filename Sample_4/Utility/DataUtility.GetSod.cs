﻿using Sample_4.Models;
using System;

namespace Sample_4.Utility
{
    /// <summary>
    /// 假設某些計算是很複雜的 (通常是可以被他人reuse的) 把他獨立出來
    /// </summary>
    public static partial class DataUtility
    {
        public static DateTime GetSod(Sales_Order so)
        {
            //假設這裡要處理一些很複雜的邏輯
            return so.Rsod.HasValue ? so.Rsod.Value : so.Sod;
        }
    }
}
