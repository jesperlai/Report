﻿using Sample_1.Models;
using Sample_1.ViewModels;
using System.Collections.Generic;

namespace Sample_1.Helper
{
    public partial class DataAccessHelper
    {
        public TriggerPointAndDataSetVM InitializeData()
        {
            var data = new TriggerPointAndDataSetVM();

            data.Tp = GetTriggerPoints();
            data.DataSet = ConcreteDataSet(data.Tp);

            return data;
        }

        private List<Lot_Txn> GetTriggerPoints()
        {
            return _repo.GetTxn(_dataStartTime, _dataEndTime);
        }
    }
}
