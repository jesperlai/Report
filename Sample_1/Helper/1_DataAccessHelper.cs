﻿using System;

namespace Sample_1.Helper
{
    public partial class DataAccessHelper
    {
        private Repository _repo;
        private DateTime _dataStartTime, _dataEndTime;

        /// <summary>
        /// 通常會傳入一些參數 為了在資料庫裡撈出一些符合的結果來寫出成報表 ex: 撈某一個區間的資料
        /// </summary>
        public DataAccessHelper(DateTime pDataStartTime, DateTime pDataEndTime)
        {
            _repo = new Repository();
            _dataStartTime = pDataStartTime;
            _dataEndTime = pDataEndTime;
        }
    }
}
