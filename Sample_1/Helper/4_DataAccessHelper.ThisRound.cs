﻿using Sample_1.Models;
using Sample_1.ViewModels;
using System.Linq;

namespace Sample_1.Helper
{
    public partial class DataAccessHelper
    {
        /// <summary>
        /// 因為希望不要一直頻繁開關DB 所以前面一次把所有批號的資料都撈出來了
        /// 接下來得篩選出只屬於這回合的資料 (例如 根據這回合的過站記錄的批號 從剛剛的DataSet中找出訂單 => 不是每跑一批就查一次DB )
        /// </summary>
        public static ThisRoundDataVM GetThisRoundData(DataSetVM src, Lot_Txn tp)
        {
            ThisRoundDataVM tRound = new ThisRoundDataVM();

            tRound.So = GetSo(src, tp);

            return tRound;
        }

        private static Sales_Order GetSo(DataSetVM src, Lot_Txn tp)
        {
            var Sos = src.So.Where(q => q.Lot_No == tp.Lot_No).ToList();
            return Sos.Any() ? Sos.First() : new Sales_Order();
        }
    }
}
