﻿using System;

namespace Sample_1.Models
{
    /// <summary>
    /// 輸出結果
    /// </summary>
    public class Report
    {
        /// <summary>
        /// 假設報表中有個欄位是輸出生產公司是誰 (固定值)
        /// </summary>
        public string Company
        {
            get
            {
                return "很棒棒工廠";
            }
        }
        /// <summary>
        /// 訂單編號
        /// </summary>
        public string Po_No { get; set; }
        /// <summary>
        /// 批號
        /// </summary>
        public string Lot_No { get; set; }
        /// <summary>
        /// 過站時間
        /// </summary>
        public DateTime Txn_Dt { get; set; }
        /// <summary>
        /// 站別
        /// </summary>
        public string Stage_Id { get; set; }
        /// <summary>
        /// 過站數量
        /// </summary>
        public int Qty { get; set; }
        /// <summary>
        /// 交期
        /// </summary>
        public DateTime? Sod { get; set; }
        /// <summary>
        /// 訂單數量
        /// </summary>
        public int Order_Qty { get; set; }
    }
}
