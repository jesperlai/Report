﻿using System;

namespace Sample_1.Models
{
    /// <summary>
    /// 交易過站資料表
    /// </summary>
    public class Lot_Txn
    {
        /// <summary>
        /// 過站時間
        /// </summary>
        public DateTime Txn_Dt { get; set; }
        /// <summary>
        /// 批號
        /// </summary>
        public string Lot_No { get; set; }
        /// <summary>
        /// 站碼
        /// </summary>
        public string Txn_Code { get; set; }
        /// <summary>
        /// 站別
        /// </summary>
        public string Stage_Id { get; set; }
        /// <summary>
        /// 過站數量
        /// </summary>
        public double? Qty { get; set; }
    }
}
