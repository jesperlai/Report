﻿using Sample_1.Models;
using System.Collections.Generic;

namespace Sample_1.ViewModels
{
    public class TriggerPointAndDataSetVM
    {
        public List<Lot_Txn> Tp { get; set; }
        public DataSetVM DataSet { get; set; }
    }
}
