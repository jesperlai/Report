﻿using Sample_1.Models;
using System.Collections.Generic;

namespace Sample_1.ViewModels
{
    public class DataSetVM
    {
        public List<Sales_Order> So { get; set; }
    }
}
