﻿using Sample_1.Extensions;
using Sample_1.Helper;
using Sample_1.Models;
using Sample_1.Utility;
using Sample_1.ViewModels;
using System;
using System.Collections.Generic;

namespace Sample_1
{
    public class Controller
    {
        private DataAccessHelper _helper;

        public Controller(DateTime pDataStartTime, DateTime pDataEndTime)
        {
            _helper = new DataAccessHelper(pDataStartTime, pDataEndTime);
        }

        public List<Report> GetData()
        {
            var src = _helper.InitializeData();
            var result = GetData(src);

            return result;
        }

        private List<Report> GetData(TriggerPointAndDataSetVM src)
        {
            var result = new List<Report>();

            //我把Trigger Point 觸發點 縮寫成tp
            foreach (var tp in src.Tp)
            {
                //該回合需用到的資料 我把this round 縮寫成 tRound
                var tRound = DataAccessHelper.GetThisRoundData(src.DataSet, tp);

                //Controller 盡量扁平 如果有複雜的運算可以放到 Utility 做
                var item = new Report
                {
                    Txn_Dt = tp.Txn_Dt,
                    Lot_No = tp.Lot_No,
                    Stage_Id = tp.Stage_Id,
                    Qty = tp.Qty.Trim(),   //有些轉型或是常用的運算 寫成extension會比較易用
                    Po_No = tRound.So.Po_No,
                    Order_Qty = tRound.So.Qty,
                    Sod = DataUtility.GetSod(tRound.So),  //假設這個欄位有很複雜的邏輯 放到Utitlity做
                };

                result.Add(item);
            }

            return result;
        }
    }
}
