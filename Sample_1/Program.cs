﻿using System;
using System.Linq;

namespace Sample_1
{
    class Program
    {
        static void Main(string[] args)
        {
            {
                //為了方便講解 (通常這兩個值會從外面傳進來)----------------------
                var sTime = DateTime.Parse("2001/1/1");
                var eTime = DateTime.Parse("2001/2/20");
                //---------------------------------------------------------------

                //組裝資料
                Controller controller = new Controller(sTime, eTime);
                var result = controller.GetData();

                //寫出檔案
                RptService myRpt = new RptService();
                result = result.OrderBy(q => q.Txn_Dt).ToList();  //最後輸出資料要以過站時間排序
                myRpt.GenFile(result, @"D:\sample.csv");
            }
        }
    }
}
