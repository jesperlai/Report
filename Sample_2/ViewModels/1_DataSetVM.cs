﻿using Sample_2.Models;
using System.Collections.Generic;

namespace Sample_2.ViewModels
{
    public class DataSetVM
    {
        public List<Sales_Order> So { get; set; }
    }
}
