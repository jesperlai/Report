﻿using Sample_2.Event;
using Sample_2.Models;
using Sample_2.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Sample_2
{
    class Program
    {
        private static BuildEventParam MakeBuildEventParam(string eventName)
        {
            return new BuildEventParam
            {
                //為了方便講解 (通常這兩個值會從外面傳進來)---------
                STime = DateTime.Parse("2001/1/1"),
                ETime = DateTime.Parse("2001/2/1"),
                //--------------------------------------------------
                EventName = eventName,
            };
        }

        static void Main(string[] args)
        {
            var result = new List<Report>();

            //進站
            BaseEvent moveIn = new Event_MoveIn(MakeBuildEventParam("這是一個進站事件"));
            result.AddRange(moveIn.Run());

            //出站
            BaseEvent moveOut = new Event_MoveOut(MakeBuildEventParam("這是一個出站事件"));
            result.AddRange(moveOut.Run());

            //寫出檔案
            RptService myRpt = new RptService();
            result = result.OrderBy(q => q.Txn_Dt).ToList();  //最後輸出資料要以過站時間排序
            myRpt.GenFile(result, @"D:\sample.csv");
        }
    }
}
