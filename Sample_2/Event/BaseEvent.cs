﻿using Sample_2.Extensions;
using Sample_2.Helper;
using Sample_2.Models;
using Sample_2.Utility;
using Sample_2.ViewModels;
using System.Collections.Generic;

namespace Sample_2.Event
{
    public abstract class BaseEvent
    {
        private DataAccessHelper _helper;
        protected Repository repo;
        protected BuildEventParam builderParam;

        public BaseEvent(BuildEventParam param)
        {
            builderParam = param;
            repo = new Repository();
            _helper = new DataAccessHelper(repo);
        }

        public List<Report> Run()
        {
            var triggerPoint = GetTriggerPoint();
            return GetData(triggerPoint);
        }

        public abstract List<Lot_Txn> GetTriggerPoint();

        public virtual List<Report> GetData(List<Lot_Txn> triggerPoints)
        {
            var dataSet = _helper.ConcreteDataSet(triggerPoints);

            var result = new List<Report>();
            //我把Trigger Point 觸發點 縮寫成tp
            foreach (var tp in triggerPoints)
            {
                //該回合需用到的資料 我把this round 縮寫成 tRound
                var tRound = DataAccessHelper.GetThisRoundData(dataSet, tp);

                //Controller 盡量扁平 如果有複雜的運算可以放到 Utility 做
                var item = new Report
                {
                    Txn_Dt = tp.Txn_Dt,
                    Lot_No = tp.Lot_No,
                    Stage_Id = tp.Stage_Id,
                    Qty = tp.Qty.Trim(),   //有些轉型或是常用的運算 寫成extension會比較易用
                    Po_No = tRound.So.Po_No,
                    Order_Qty = tRound.So.Qty,
                    Sod = DataUtility.GetSod(tRound.So),  //假設這個欄位有很複雜的邏輯 放到Utitlity做
                    Event_Name = builderParam.EventName,
                };

                result.Add(item);
            }

            return result;
        }
    }
}
