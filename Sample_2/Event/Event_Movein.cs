﻿using Sample_2.Models;
using Sample_2.ViewModels;
using System.Collections.Generic;

namespace Sample_2.Event
{
    public class Event_MoveIn : BaseEvent
    {
        public Event_MoveIn(BuildEventParam param) : base(param)
        {

        }

        public override List<Lot_Txn> GetTriggerPoint()
        {
            var txns = repo.GetTxn(builderParam.STime, builderParam.ETime, "進站");

            return txns;
        }

        public override List<Report> GetData(List<Lot_Txn> triggerPoints)
        {
            var result = base.GetData(triggerPoints);

            //處理一些該 event 特別不同於 base event 的地方
            foreach (var item in result)
            {
                item.Qty += 1;   //隨便舉例 假設進站數量都固定要 + 1 

                //通常還會有更多更多相異處
                //通常還會有更多更多相異處
                //通常還會有更多更多相異處
                //通常還會有更多更多相異處
            }

            return result;
        }
    }
}
