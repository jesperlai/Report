﻿using Sample_2.Models;
using Sample_2.ViewModels;
using System.Collections.Generic;
using System.Linq;

namespace Sample_2.Helper
{
    public partial class DataAccessHelper
    {
        /// <summary>
        /// 其他資料都是based on 你觸發點撈出來的資料有什麼 再去補撈其他資料 
        /// 例如: 先撈出過站記錄，再透過這段時間內有哪些貨批在動 (表示要報) 再去撈出這些貨批的客戶訂單資料
        /// </summary>
        public DataSetVM ConcreteDataSet(List<Lot_Txn> triggerPoints)
        {
            DataSetVM dataSet = new DataSetVM();

            dataSet.So = GetSo(triggerPoints);

            return dataSet;
        }

        private List<Sales_Order> GetSo(List<Lot_Txn> triggerPoints)
        {
            var lotNos = triggerPoints.Select(q => q.Lot_No).Distinct().ToList();
            var result = _repo.GetSo(lotNos);

            return result;
        }
    }
}
